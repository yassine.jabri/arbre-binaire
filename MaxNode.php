<?php
namespace foo;

/**
 * Class MaxNode
 * @package foo
 */
class MaxNode
{
    /**
     * @param Node $node
     * @return int
     */
    public static function findMax(Node $node) : int
    {
        $leftMaxvalue = $node->getNodeLeft() != null ? self::findMax($node->getNodeLeft()) : rand(0, $node->getData());
        $rightMaxValue = $node->getNodeRight() != null ? self::findMax($node->getNodeRight()) : rand($node->getData(), 0);
        if($node->getData() > $leftMaxvalue && $node->getData() >$rightMaxValue){
            return $node->getData();
        }
        if($leftMaxvalue>$rightMaxValue){
            return $leftMaxvalue;
        }
        return $rightMaxValue;

    }
}
?>