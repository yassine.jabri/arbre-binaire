<?php

namespace foo;
include ("Node.php");
include ('MaxNode.php');

$root = new Node(15);
$node1 = new Node(2);
$node2 = new Node(8);
$node3 = new Node(19);
$root->setNodeLeft($node1);
$root->setNodeRight($node2);
$node1->setNodeLeft($node3);
$result = MaxNode::findMax($root);
echo 'result ' . $result;
