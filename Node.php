<?php
namespace foo;

/**
 * Class Node
 * @package foo
 */
class Node
{
    /**
     * @var int
     */
    private int $data;
    /**
     * @var \foo\Node|null
     */
    private ?Node $nodeLeft =null;
    /**
     * @var Node|null
     */
    private ?Node $nodeRight=null;

    /**
     * @return int
     */
    public function getData(): int
    {
        return $this->data;
    }

    /**
     * @param int $data
     */
    public function setData(int $data): void
    {
        $this->data = $data;
    }

    /**
     * @return Node|null
     */
    public function getNodeLeft(): ?Node
    {
        return $this->nodeLeft;
    }

    /**
     * @param Node|null $nodeLeft
     */
    public function setNodeLeft(?Node $nodeLeft): void
    {
        $this->nodeLeft = $nodeLeft;
    }

    /**
     * @return Node|null
     */
    public function getNodeRight(): ?Node
    {
        return $this->nodeRight;
    }

    /**
     * @param Node|null $nodeRight
     */
    public function setNodeRight(?Node $nodeRight): void
    {
        $this->nodeRight = $nodeRight;
    }

    function __construct($data)
    {
        $this->data = $data;
    }

}